import 'package:flutter/cupertino.dart';
import 'package:uikit/core/delegate/uikit_delegate.dart';
import 'package:uikit/core/delegate/uikit_param.dart';

class DelegatesComposer extends StatelessWidget {
  final List<UiKitDelegate> delegates;
  final List<UiKitParam> params;

  DelegatesComposer(this.delegates, {required this.params});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: params.length,
        itemBuilder: (BuildContext context, int index) {
          var param = params[index];
          final delegate = _findDelegate(param);
          if (delegate == null) {
            throw Exception('Can`t find proper delegate for params: $params');
          }

          return delegate.getView(param);
        });
  }

  UiKitDelegate? _findDelegate(UiKitParam param) {
    for (final delegate in delegates) {
      if (delegate.doesItFit(param)) {
        return delegate;
      }
    }

    return null;
  }
}
