import 'package:flutter/widgets.dart';
import 'package:uikit/core/delegate/uikit_delegate.dart';
import 'package:uikit/core/delegate/uikit_param.dart';

class TransactionDelegateParams extends UiKitParam {
  final name;

  TransactionDelegateParams({String id = '', required this.name}) : super(id: id);
}

enum PaddinValues {
  padding_8, padding_16
}

class TransactionDelegate extends UiKitDelegate<TransactionDelegateParams> {
  @override
  Widget getView(TransactionDelegateParams param) {
    return Text('Transaction: ${param.name}');
  }

}