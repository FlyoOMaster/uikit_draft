import 'package:flutter/material.dart';
import 'package:uikit/core/delegate/uikit_delegate.dart';
import 'package:uikit/core/delegate/uikit_param.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/attributes/align.dart';

class ButtonDelegateParams extends UiKitParam {
  final String text;
  final AlignUiKit align;

  ButtonDelegateParams({String id = '', required this.text, this.align = AlignUiKit.left})
      : super(id: id);
}

class ButtonDelegate extends UiKitDelegate<ButtonDelegateParams> {
  final Function(String)? onPressed;

  ButtonDelegate({this.onPressed});

  @override
  Widget getView(ButtonDelegateParams param) {
    final alignment;
    switch(param.align) {
      case AlignUiKit.center:
        alignment = Alignment.center;
        break;

      case AlignUiKit.right:
        alignment = Alignment.centerRight;
        break;
      default:
        alignment = Alignment.centerLeft;
        break;
    }
    return Align(
      alignment: alignment,
      child: ElevatedButton(
          key: ValueKey(param.id),
          // style: ButtonStyle(tapTargetSize: MaterialTapTargetSize.shrinkWrap),
          onPressed: () {
            onPressed?.call(param.id);
          },
          child: Text(param.text)),
    );
  }
}
