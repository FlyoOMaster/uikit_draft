import 'package:flutter/cupertino.dart';
import 'package:uikit/core/delegate/uikit_delegate.dart';
import 'package:uikit/core/delegate/uikit_param.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/transaction_delegate.dart';

class TransactionsDelegateParams extends UiKitParam {
  final List<TransactionDelegateParams> transactions;

  TransactionsDelegateParams({
    String id = '',
    required this.transactions,
  }) : super(id: id);
}

class TransactionsDelegate extends UiKitDelegate<TransactionsDelegateParams> {
  final TransactionDelegate _delegate = TransactionDelegate();

  @override
  Widget getView(TransactionsDelegateParams param) {
    return Column(
      children: [
        Text(
          'Transaction list',
          style: TextStyle(fontSize: 32),
        ),
        Padding(padding: EdgeInsets.only(top: 30)),
        ListView.builder(
            shrinkWrap: true,
            itemCount: param.transactions.length,
            itemBuilder: (_, i) {
              return _delegate.getView(param.transactions[i]);
            })
        // SizedBox(
        //   height: 300,
        //   child: ListView.builder(
        //     itemCount: param.transactions.length,
        //       itemBuilder: (_, i) {
        //         return _delegate.getView(param.transactions[i]);
        //   }),
        // )
      ],
    );
  }
}
