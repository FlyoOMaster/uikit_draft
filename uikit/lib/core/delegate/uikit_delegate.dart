import 'package:flutter/widgets.dart';
import 'package:uikit/core/delegate/uikit_param.dart';

///
/// Base delegate class that contain all common functionality
/// [UiKitParam] represent require parameters for the delegate
abstract class UiKitDelegate<T extends UiKitParam> {
  ///
  /// Check does this params suits for this delegate
  /// [param] - params to check
  bool doesItFit(Object param) => param is T;

  ///
  /// Returns a view of the delegate
  Widget getView(T param);
}