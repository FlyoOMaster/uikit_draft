///
/// Base class for every param of the [UiKitDelegate]
///
abstract class UiKitParam {
  /// Identifier of this particular param
  final String id;

  UiKitParam({required this.id});
}