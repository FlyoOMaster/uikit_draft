import 'dart:async';

import 'package:uikit/core/delegate/uikit_param.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/attributes/align.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/button_delegate.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/transaction_delegate.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/transactions_delegate.dart';

class ViewModel {
  final _stateStreamController = StreamController<List<UiKitParam>>();

  Stream<List<UiKitParam>> get stateStream => _stateStreamController.stream;

  ViewModel() {
    _changeState();
  }
  _changeState() async {



    final transactions = TransactionsDelegateParams(transactions: [
      TransactionDelegateParams(name: 'First transaction'),
      TransactionDelegateParams(name: 'Second transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'First transaction'),
      TransactionDelegateParams(name: 'Second transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'First transaction'),
      TransactionDelegateParams(name: 'Second transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'First transaction'),
      TransactionDelegateParams(name: 'Second transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'First transaction'),
      TransactionDelegateParams(name: 'Second transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'First transaction'),
      TransactionDelegateParams(name: 'Second transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'Third transaction'),
      TransactionDelegateParams(name: 'First transaction'),
    ]);
    _stateStreamController.add([transactions]);
    await Future.delayed(Duration(seconds: 5));

    _stateStreamController.sink.add([
      transactions,
      ButtonDelegateParams(
        id: 'first_button',
        text: 'First button',
        align: AlignUiKit.left,
      ),
      ButtonDelegateParams(
        id: 'second_button',
        text: 'Second button',
        align: AlignUiKit.right,
      )
    ]);

    await Future.delayed(Duration(seconds: 5));

    _stateStreamController.sink.add(
        [
          ButtonDelegateParams(
            id: 'third_button',
            text: 'Third button',
            align: AlignUiKit.center,
          ),
          ButtonDelegateParams(
            id: 'four_button',
            text: 'Four button',
          )
        ]
    );

    await Future.delayed(Duration(seconds: 5));

    _stateStreamController.add(
        [
          ButtonDelegateParams(
            id: 'five_button',
            text: 'Five button',
          ),
          ButtonDelegateParams(
            id: 'six_button',
            text: 'Six button',
          )
        ]
    );
  }

  onButtonPressed(String id) {
    print('Button click: $id');
  }

  dispose() {
    _stateStreamController.close();
  }
}