import 'package:example/view_mode.dart';
import 'package:flutter/material.dart';
import 'package:uikit/core/delegate/uikit_param.dart';
import 'package:uikit/delegate_composer/delegate_composer.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/attributes/align.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/button_delegate.dart';
import 'package:uikit/delegates/tmp_just_for_implementation_purpose/transactions_delegate.dart';

void main() {
  runApp(UikitApp());
}

class UikitApp extends StatefulWidget {
  const UikitApp({Key? key}) : super(key: key);

  @override
  _UikitAppState createState() => _UikitAppState();
}

class _UikitAppState extends State<UikitApp> {
  late ButtonDelegate _buttonDelegate;

  late ViewModel viewModel;

  _UikitAppState() {
    _buttonDelegate = ButtonDelegate(
      onPressed: (id) {
        viewModel.onButtonPressed(id);
      },
    );
  }

  @override
  void initState() {
    super.initState();
    viewModel = ViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UiKit test',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Uikit test'),
        ),
        body: Stack(
          children: [
            Positioned.fill(
              child: StreamBuilder<List<UiKitParam>>(
                stream: viewModel.stateStream,
                initialData: [],
                builder: (_, snapshot) {
                  return DelegatesComposer(
                    [
                      TransactionsDelegate(),
                      ButtonDelegate(
                        onPressed: (id) {
                          viewModel.onButtonPressed(id);
                        },
                      ),
                    ],
                    params: snapshot.data ?? [],
                  );
                },
              ),
            ),
            Positioned(
              bottom: 30,
              left: 0,
              right: 0,
              child: _buttonDelegate.getView(
                ButtonDelegateParams(
                    text: "Bottom button. Yahoo", align: AlignUiKit.center),
              ),
            )
          ],
        ),
      ),
    );
  }
}
